from nba_constants import US_STATE_LIST, FOREIGN_PLAYER_STRING, LOCAL_PLAYER_STRING, TEAM_RECORDS_FILE, \
    PLAYER_PROFILES_FILE, PLAYER_SALARIES_FILE, PLAYER_STATS_FILE, PLAYER_STATS_FULL_FILE, PLAYER_DATA_OUTPUT_FILE, \
    TEAM_SYMBOL_MAP, TEAM_STATS_OUTPUT_FILE, TEAM_LIST, POSITION_DATA_OUTPUT_FILE

COMMON_PLAYER_DICT = {}

def to_float(ip_str):
    try:
        return round(float(ip_str), 3)
    except ValueError:
        return 0

def get_foreign_player_and_salary_pct(player_list, salary_list, player_profile_data):

    total_salary = sum(salary_list)
    salary_dict = {1:[], 0:[]}
    player_birth_data = []
    el_num = 0
    for player in player_list:
        player_split_list = player.split()
        if len(player_split_list) > 2:
            player_name = " ".join(player_split_list[:2])
        else:
            player_name = player
        try:

            if player_profile_data[player_name][2] == FOREIGN_PLAYER_STRING:
                player_birth_data.append(1)
                salary_dict[1].append(salary_list[el_num])
            else:
                player_birth_data.append(0)
                salary_dict[0].append(salary_list[el_num])
        except KeyError:
            player_birth_data.append(0)
            salary_dict[0].append(salary_list[el_num])

        el_num += 1

    return round(float(sum(player_birth_data))/len(player_birth_data), 2), round(float(sum(salary_dict[1]))/total_salary, 2)

def get_player_avg_stats(player_profiles=None, player_salary_data=None):

    with open(PLAYER_STATS_FILE) as f_season_stats:
        season_stats = f_season_stats.read().split("\n")

    fields = season_stats[0].split(",")
    # print fields

    player_stats = {}
    playing_position_stats = {'usa':{}, 'foreign':{}, "error":{}}
    for stat in season_stats:
        stat_list = stat.split(",")
        player = stat_list[2].replace('"','').lower().replace("*", "")
        play_pos = stat_list[3].replace('"','').lower().replace("*", "")

        if player in player_salary_data:
            player_salary_coeff = player_salary_data[player][0]
        else:
            player_salary_coeff = -10
        stat_list.append(player_salary_coeff)

        if player not in player_stats:
            player_stats[player] = {}

        if player in player_profile_data:
            country = player_profile_data[player][2]
        else:
            country = "error"
        if play_pos not in playing_position_stats:
            playing_position_stats[country][play_pos] = {}
        year = stat_list[1]

        if year not in playing_position_stats[country][play_pos]:
            playing_position_stats[country][play_pos][year] = []
        player_stats[player][year] = map(to_float, stat_list[6:])
        playing_position_stats[country][play_pos][year].append(map(to_float, stat_list[6:]))
        # playing_position_stats[country][play_pos][year].append(player_salary_coeff)

    playing_pos_yearly_avg_stats = {}
    for country in playing_position_stats:
        playing_pos_yearly_avg_stats[country] = {}
        for position in playing_position_stats[country]:
            playing_pos_yearly_avg_stats[country][position] = {}
            for year in playing_position_stats[country][position]:
                year_stats = playing_position_stats[country][position][year]
                print year_stats
                playing_pos_yearly_avg_stats[country][position][year] = [round(float(sum(col))/len(col),3) for col in zip(*year_stats)]

    avg_player_stats = {}
    for player in player_stats:
        if len(player) == 0:
            continue
        player_year_stats = player_stats[player]
        # print player_year_stats
        all_stats = []
        for year in player_year_stats:
            all_stats.append(player_year_stats[year])
        # print all_stats
        avg_stats = [round(float(sum(col))/len(col),3) for col in zip(*all_stats)]
        avg_player_stats[player] = avg_stats
        num_seasons = len(all_stats)
        avg_player_stats[player].append(num_seasons)

    avg_position_stats = {}
    for country in playing_pos_yearly_avg_stats:
        avg_position_stats[country] = {}
        for position in playing_pos_yearly_avg_stats[country]:
            if len(position) == 0:
                continue
            position_year_stats = playing_pos_yearly_avg_stats[country][position]
            # print player_year_stats
            all_stats = []
            for year in position_year_stats:
                all_stats.append(position_year_stats[year])
            # print all_stats
            # print all_stats
            avg_stats = [round(float(sum(col))/len(col),3) for col in zip(*all_stats)]
            avg_position_stats[country][position] = avg_stats
            num_seasons = len(all_stats)
            avg_position_stats[country][position].append(num_seasons)

    headers = []
    headers.extend(fields[6:])
    headers.append("num_seasons")

    return avg_player_stats, headers, avg_position_stats

def get_player_profiles():
    with open(PLAYER_PROFILES_FILE) as f_player_profiles:
        player_profiles = f_player_profiles.read().split("\n")

    fields =  player_profiles[0].split(",")
    headers = fields[2:4]
    headers.append(fields[7])

    player_profile_data = {}
    for profile in player_profiles[1:]:
        profile_data = profile.split(",")
        player = profile_data[1].replace('"','').lower().replace("*", "")
        player_profile_data[player] = profile_data[2:4]
        if len(profile_data) == len(fields):
            birth_state = profile_data[7].replace("\r","").lower()
        else:
            birth_state = profile_data[8].replace("\r","").lower()
        if birth_state in US_STATE_LIST:
            player_country = LOCAL_PLAYER_STRING
        else:
            player_country = FOREIGN_PLAYER_STRING

        player_profile_data[player].append(player_country)

    return player_profile_data, headers

def get_player_salaries(team_yearly_data):
    with open(PLAYER_SALARIES_FILE) as f_player_salaries:
        player_salaries = f_player_salaries.read().split("\n")

    fields =  player_salaries[0].split(",")
    # headers = fields[1:]
    headers = ["avg_salary_coeff"]

    player_salary_data = {}
    for salary_str in player_salaries[1:]:
        if len(salary_str) == 0:
            continue
        salary_data = salary_str.split(",")
        player = salary_data[0].replace('"','').lower().replace("*", "")
        if player not in player_salary_data:
            player_salary_data[player] = []
        salary = float(salary_data[1])
        year = int(salary_data[3])
        team = salary_data[4].strip().replace('"','').replace("*", "")
        max_salary_for_year = team_yearly_data[team][year][0]
        salary_coeff = round(float(salary/max_salary_for_year), 3)
        player_salary_data[player].append(salary_coeff)
        # if player == "manu ginobili":
        # if player == "kobe bryant":
        #     print salary, max_salary_for_year, year, salary_coeff

    # print player_salary_data["manu ginobili"]
    # print player_salary_data["kobe bryant"]

    player_salary_avg_data = {}
    for player in player_salary_data:
        player_salary_avg_data[player] = [round(sum(player_salary_data[player])/len(player_salary_data[player]) ,3)]

    return player_salary_avg_data, headers

def get_team_record_missing_data(team_record_data, team, year):

    max_years_back = 3
    try:
        return team_record_data[team][year-1]

    except KeyError:
        decr = 2
        while decr <= max_years_back:
            new_year = year - decr
            if new_year in team_record_data[team]:
                return team_record_data[team][new_year]
            else:
                decr += 1

    if team == "CHA" and (year < 2004 or year > 2013):
        if year == 2017:
            return team_record_data["NOH"][year-1]
        return team_record_data["NOH"][year]

    raise

def get_team_yearly_record_data():
    with open(TEAM_RECORDS_FILE) as f_team_records:
        team_records = f_team_records.read().split("\n")

    team_record_data = {}
    for team_record in team_records[1:]:
        record_data = team_record.split(",")
        # print record_data, type(record_data)
        team_full_name = record_data[1].strip()
        if len(team_full_name) == 0:
            continue
        team = TEAM_SYMBOL_MAP[team_full_name]
        if team not in team_record_data:
            team_record_data[team] = {}
        year = int(record_data[4].strip())
        team_record_data[team][year] = round(float(record_data[3]), 3)

    return team_record_data

def get_team_data(player_profile_data):
    with open(PLAYER_SALARIES_FILE) as f_player_salaries:
        player_salaries = f_player_salaries.read().split("\n")

    # fields =  player_salaries[0].split(",")
    headers = ["max_salary", "foreign_player_pct", "win_pct"]

    team_record_data = get_team_yearly_record_data()
    team_data = {}
    for salary_str in player_salaries[1:]:
        if len(salary_str) == 0:
            continue
        salary_data = salary_str.split(",")
        team = salary_data[4].strip().replace('"','').replace("*", "")
        year = int(salary_data[3].strip())
        salary = int(salary_data[1].strip())
        player = salary_data[0].replace('"','').lower().replace("*", "")
        if team not in team_data:
            team_data[team] = {}
        if year not in team_data[team]:
            team_data[team][year] = {}
            team_data[team][year]["salaries"] = []
            team_data[team][year]["players"] = []
        team_data[team][year]["salaries"].append(int(salary))
        team_data[team][year]["players"].append(player)

    team_agg_data = {}
    for team in team_data:
        team_year_data = team_data[team]
        if team not in team_agg_data:
            team_agg_data[team] = {}
        for year in team_year_data:
            players = team_year_data[year]["players"]
            salaries = team_year_data[year]["salaries"]
            if year in team_record_data[team]:
                year_win_pct = team_record_data[team][year]
            else:
                year_win_pct = get_team_record_missing_data(team_record_data, team, year)

            foreign_player_pct, foreign_salary_pct = \
                get_foreign_player_and_salary_pct(players, salaries, player_profile_data)

            team_agg_data[team][year] = [max(salaries),
                                         sum(salaries),
                                         foreign_player_pct,
                                         foreign_salary_pct,
                                         year_win_pct]

    return team_agg_data, headers

def consolidate_player_data(**data_objects):
    consolidated_player_data = {}
    player_headers = {}
    for arg, data_tuple in data_objects.items():
        player_data = data_tuple[0]
        header_data = data_tuple[1]
        for player in player_data:
            # if player not in COMMON_PLAYER_DICT:
            #     continue
            #     print "player: {0}".format(player)

            if player not in consolidated_player_data:
                consolidated_player_data[player] = {}
                player_headers[player] = []

            for i in range(len(header_data)):
                # consolidated_player_data[player].extend(player_data[player])
                consolidated_player_data[player][header_data[i].replace("\r","")] = player_data[player][i]
            player_headers[player].extend(header_data)

    return consolidated_player_data, player_headers

# def get_team_avg_stats_by_year():
#
#     with open(PLAYER_STATS_FILE) as f_season_stats:
#         season_stats = f_season_stats.read().split("\n")
#
#     fields = season_stats[0].split(",")
#     headers = ["year", ]
#
#     team_stats = {}
#     for stat in season_stats:
#         stat_list = stat.split(",")
#         team = stat_list[5].replace('"','').replace("*", "")
#         if team not in TEAM_LIST:
#             continue
#         if team not in team_stats:
#             team_stats[team] = {}
#         year = stat_list[1]
#         team_stats[team][year] = map(to_float, stat_list[6:])
#
#     avg_team_stats = {}
#     for player in team_stats:
#         if len(player) == 0:
#             continue
#         player_year_stats = player_stats[player]
#         # print player_year_stats
#         all_stats = []
#         for year in player_year_stats:
#             all_stats.append(player_year_stats[year])
#         # print all_stats
#         avg_stats = [round(float(sum(col))/len(col),3) for col in zip(*all_stats)]
#         avg_player_stats[player] = avg_stats
#         num_seasons = len(all_stats)
#         avg_player_stats[player].append(num_seasons)
#
#     return avg_team_stats, headers


if __name__ == "__main__":

    player_profile_data, profile_headers = get_player_profiles()
    team_yearly_data, team_data_headers = get_team_data(player_profile_data)
    player_salary_avg_data, salary_headers = get_player_salaries(team_yearly_data)
    player_avg_stats, avg_stats_headers, avg_position_stats = get_player_avg_stats(player_profile_data, player_salary_avg_data)

    # print len(player_avg_stats), len(player_profile_data), len(player_salary_avg_data)
    kwargs = {"ds1": (player_avg_stats, avg_stats_headers), "ds2": (player_profile_data, profile_headers), "ds3": (player_salary_avg_data, salary_headers)}

    consolidated_player_data, player_headers =  consolidate_player_data(**kwargs)
    print "total {0} players".format(len(consolidated_player_data))

    full_cnt = 0
    print_cnt = 0
    cnt = 1
    for player in consolidated_player_data:
        if cnt <= print_cnt:
            print player
            print consolidated_player_data[player]
        cnt += 1
        num_fields = len(consolidated_player_data[player].keys())
        headers = player_headers[player]
        if num_fields != len(headers):
            print player
        else:
            full_cnt += 1

    print "{0} players have all data".format(full_cnt)

    # for country in avg_position_stats:
    #     for position in avg_position_stats[country]:
    #         print avg_position_stats[country][position]

    all_headers = []
    max_headers = 52
    for player in player_headers:
        header_cnt = len(player_headers[player])
        if header_cnt == max_headers:
            all_headers = player_headers[player]
            break

    # for header in all_headers:
    #     print header
    # print len(all_headers)

    with open(PLAYER_DATA_OUTPUT_FILE, "w") as f_data_dump:
        # header
        data_str = "player"
        for i in range(len(all_headers)):
            data_str += ",{0}".format(all_headers[i].replace("\r",""))

        print data_str
        f_data_dump.write("{0}\n".format(data_str))

        # data
        for player in consolidated_player_data:
            data_str = player
            for i in range(len(all_headers)):
                try:
                    data_value = consolidated_player_data[player][all_headers[i].replace("\r","")]
                except KeyError:
                    data_value = ""

                data_str += ",{0}".format(data_value)
            f_data_dump.write("{0}\n".format(data_str))

    print "player data written to {0}".format(PLAYER_DATA_OUTPUT_FILE)

    with open(TEAM_STATS_OUTPUT_FILE, "w") as f_data_dump:
        # header
        data_str = "team,year,max_salary,total_salary,foreign_pct,foreign_salary_pct,win_pct"
        f_data_dump.write("{0}\n".format(data_str))

        # data
        for team in team_yearly_data:
            for year in team_yearly_data[team]:
                data_str = "{0},{1},{2},{3},{4},{5},{6}".format(team, year, team_yearly_data[team][year][0],
                                                                team_yearly_data[team][year][1],
                                                                team_yearly_data[team][year][2],
                                                                team_yearly_data[team][year][3],
                                                                team_yearly_data[team][year][4])
                f_data_dump.write("{0}\n".format(data_str))

    print "team data written to {0}".format(TEAM_STATS_OUTPUT_FILE)

    with open(POSITION_DATA_OUTPUT_FILE, "w") as f_data_dump:
        # header
        data_str = "country,position"
        for i in range(len(avg_stats_headers)):
            data_str += ",{0}".format(avg_stats_headers[i].replace("\r",""))
        data_str += ",sal_coeff"

        print data_str
        f_data_dump.write("{0}\n".format(data_str))

        # data
        for country in avg_position_stats:
            for position in avg_position_stats[country]:
                data_str = "{0},{1}".format(country, position)
                for i in range(len(avg_stats_headers)):
                    data_value = avg_position_stats[country][position][i]

                    data_str += ",{0}".format(data_value)
                data_str += ",{0}".format(avg_position_stats[country][position][i+1])
                f_data_dump.write("{0}\n".format(data_str))

    print "position data written to {0}".format(POSITION_DATA_OUTPUT_FILE)